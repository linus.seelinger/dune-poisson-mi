add_executable("dune-poisson-mi" dune-poisson-mi.cc)
target_link_dune_default_libraries("dune-poisson-mi")

dune_symlink_to_source_files(FILES randomfield2d.ini perm.field)
