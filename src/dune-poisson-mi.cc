#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <vector>
#include <map>
#include <string>

#include <dune/common/filledarray.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/make_array.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/istl/umfpack.hh>

#include <dune/pdelab.hh>

#include <dune/randomfield/randomfield.hh>
#include <dune/common/shared_ptr.hh>

// Server includes
#include <iostream>

#include <string>

//#include <resolv.h> // Header included in httplib.h, causing potential issues with Eigen!

#include "HTTPComm.h"

#include <chrono>
#include <thread>


template<typename DF, typename RF, unsigned int dimension>
class GridTraits
{
  public:

    enum {dim = dimension};

    typedef RF                        RangeField;
    typedef Dune::FieldVector<RF,1>   Scalar;
    typedef DF                        DomainField;
    typedef Dune::FieldVector<DF,dim> Domain;
};

const int measurement_per_dim = 5;

//===============================================================
//===============================================================
// Solve the Poisson equation
//           - \Delta u = f in \Omega,
//                    u = g on \partial\Omega_D
//  -\nabla u \cdot \nu = j on \partial\Omega_N
//===============================================================
//===============================================================

//===============================================================
// Define parameter functions f,g,j and \partial\Omega_D/N
//===============================================================

template<typename GV, typename RF>
class PoissonModelProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

  typedef GridTraits<double,double,2> MyGridTraits;
  typedef Dune::RandomField::RandomFieldList<MyGridTraits> List;
  std::shared_ptr<List::SubRandomField> field;
  typedef List::SubRandomField::Traits RndFieldTraits;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  PoissonModelProblem(const GV& gv, std::vector<std::reference_wrapper<const Eigen::VectorXd>> const& parameters, std::array<int,2> N) {
    // Generate field
    Dune::ParameterTree config;
    Dune::ParameterTreeParser parserConfig;
    parserConfig.readINITree("randomfield2d.ini",config);

    config["grid.cells"] = std::to_string(N[0]) + " " + std::to_string(N[1]);
    //std::cout << config["grid.cells"] << std::endl;

    const int dim = 2;
    List randomFieldList(config, "", Dune::RandomField::DefaultLoadBalance<dim>(), MPI_COMM_WORLD);//, comm->GetMPICommunicator());
    //randomFieldList.generate(true);

    field = randomFieldList.get("perm");

//field->generate(true);
    field->generate(42, [&](fftw_complex* extendedField, const auto* matrix, std::shared_ptr<RndFieldTraits> traits) {

      const std::array<unsigned int,RndFieldTraits::dim>& extendedCells = matrix->getExtendedCells();
      const std::array<unsigned int,RndFieldTraits::dim>& localExtendedCells = matrix->getLocalExtendedCells();
      const std::array<unsigned int,RndFieldTraits::dim>& localExtendedOffset = matrix->getLocalExtendedOffset();

      for (unsigned int index = 0; index < matrix->getLocalExtendedDomainSize(); index++)
      {
        extendedField[index][0] = 0.0;
        extendedField[index][1] = 0.0;
      }

      int c = 0;
      for (int l = 0; (parameters[0].get()).rows() > c; l++) {
        //for (int l = 0; l <= PARAM_SIZE[index->GetValue(0)]; l++) {
        for (int y = 0; y <= l; y++) {
          for (int x = -l; x <= l; x++) {
            if (x == 0 && y == 0)
              continue;

            if (std::abs(y) != l && std::abs(x) != l) // Only add new layers
              continue;
            if (y == 0 && x < 0)
              continue;

            {
              int rx = x, ry = y;

              if (rx < 0)
                rx += extendedCells[0];
              if (ry < 0)
                ry += extendedCells[1];

              rx -= localExtendedOffset[0];
              ry -= localExtendedOffset[1];

              if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

                std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
                unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

                //index =
                //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
                assert ((parameters[0].get()).rows() > c);
                assert (index < matrix->getLocalExtendedDomainSize());
                assert (index >= 0);

                double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize() / 2);

                extendedField[index][0] = lambda * (parameters[0].get())(c);
                extendedField[index][1] = -lambda * (parameters[0].get())(c);
              }
            }

            {
              int rx = -x, ry = -y;

              if (rx < 0)
                rx += extendedCells[0];
              if (ry < 0)
                ry += extendedCells[1];

              rx -= localExtendedOffset[0];
              ry -= localExtendedOffset[1];

              if (rx >= 0 && ry >= 0 && rx < localExtendedCells[0] && ry < localExtendedCells[1]) {

                std::array<unsigned int,RndFieldTraits::dim> indices = {(unsigned int)rx, (unsigned int)ry};
                unsigned int index = traits->indicesToIndex(indices, matrix->getLocalExtendedCells());

                //index =
                //std::cout << "writing #" << c << "\t at" << "\t" << x << "\t" << y << "\t" << rx << "\t" << ry << "\tidx " << index << "\t val:" << parameters.at(0)(c) << std::endl;
                assert ((parameters[0].get()).rows() > c);
                assert (index < matrix->getLocalExtendedDomainSize());
                assert (index >= 0);

                double lambda = std::sqrt(std::abs(matrix->fftTransformedMatrix[index][0]) / matrix->getExtendedDomainSize() / 2);

                extendedField[index][0] = lambda * (parameters[0].get())(c);
                extendedField[index][1] = -lambda * (parameters[0].get())(c);
              }
            }

            c++;

          }
        }
      }
      if ((parameters[0].get()).rows() != c) {
        std::cout << "ERROR! Parameter dimension mismatch!" << std::endl;
      }

    }, true);

    field->writeToVTK("field", gv);
  }

  //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
  static constexpr bool permeabilityIsConstantPerCell()
  {
    return false;
  }


  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);

    Dune::FieldVector<double, 1> field_val;
    field->evaluate(xglobal,field_val);
    double perm = std::exp(field_val[0]);

    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? perm : 0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    //const auto& xglobal = e.geometry().global(x);
    return 0.0;
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(x);
    if (xglobal[0] < 1e-6 || xglobal[0] > 1.0 - 1e-6) {
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    }
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    if (xglobal[0] > 1.0 - 1e-6) {
      return 1.0;
    }
    return 0;
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
};

//===============================================================
// Problem setup and solution
//===============================================================

// generate a P1 function and output it
template<typename GV, typename FEM, typename CON>
std::pair<std::vector<double>, double> poisson (const GV& gv, const FEM& fem, std::string filename, int q, std::vector<std::reference_wrapper<const Eigen::VectorXd>> const& parameters, std::array<int,2> N)
{
  using Dune::PDELab::Backend::native;

  // constants and types
  typedef typename FEM::Traits::FiniteElementType::Traits::
    LocalBasisType::Traits::RangeFieldType R;

  // make function space
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,
    Dune::PDELab::ISTL::VectorBackend<> > GFS;
  GFS gfs(gv,fem);
  gfs.name("solution");

  // make model problem
  typedef PoissonModelProblem<GV,R> Problem;
  Problem problem(gv, parameters, N);

  // make constraints map and initialize it from a function
  typedef typename GFS::template ConstraintsContainer<R>::Type C;
  C cg;
  cg.clear();
  Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem);
  Dune::PDELab::constraints(bctype,gfs,cg);

  // make local operator
  typedef Dune::PDELab::ConvectionDiffusionFEM<Problem,FEM> LOP;
  LOP lop(problem);

#ifdef OLD_BACKEND
  typedef Dune::PDELab::ISTLMatrixBackend MBE;
  MBE mbe;
#else
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(27); // 27 is too large / correct for all test cases, so should work fine
#endif

  // make grid operator
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,
                                     MBE,
                                     double,double,double,
                                     C,C> GridOperator;
  GridOperator gridoperator(gfs,cg,gfs,cg,lop,mbe);

  // make coefficent Vector and initialize it from a function
  // There is some weird shuffling around here - please leave it in,
  // it's there to test the copy constructor and assignment operator of the
  // matrix wrapper
  typedef typename GridOperator::Traits::Domain DV;
  DV x0(gfs,Dune::PDELab::Backend::unattached_container());
  {
    DV x1(gfs);
    DV x2(x1);
    x2 = 0.0;
    x0 = x1;
    x0 = x2;
  }

  // initialize DOFs from Dirichlet extension
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> G;
  G g(gv,problem);
  Dune::PDELab::interpolate(g,gfs,x0);
  Dune::PDELab::set_nonconstrained_dofs(cg,0.0,x0);


  // represent operator as a matrix
  // There is some weird shuffling around here - please leave it in,
  // it's there to test the copy constructor and assignment operator of the
  // matrix wrapper
  typedef typename GridOperator::Traits::Jacobian M;
  M m(gridoperator);
  gridoperator.jacobian(x0,m);
  //  Dune::printmatrix(std::cout,m.base(),"global stiffness matrix","row",9,1);

  // evaluate residual w.r.t initial guess
  typedef typename GridOperator::Traits::Range RV;
  RV r(gfs);
  r = 0.0;
  gridoperator.residual(x0,r);

  // make ISTL solver
  /*Dune::MatrixAdapter<typename M::Container,typename DV::Container,typename RV::Container> opa(native(m));
  //ISTLOnTheFlyOperator opb(gridoperator);
  Dune::SeqSSOR<typename M::Container,typename DV::Container,typename RV::Container> ssor(native(m),1,1.0);
  Dune::SeqILU<typename M::Container,typename DV::Container,typename RV::Container> ilu0(native(m),1.0);
  Dune::Richardson<typename DV::Container,typename RV::Container> richardson(1.0);

  Dune::CGSolver<typename DV::Container> solvera(opa,ilu0,1E-10,5000,2);
  // FIXME: Use ISTLOnTheFlyOperator in the second solver again
  Dune::CGSolver<typename DV::Container> solverb(opa,richardson,1E-10,5000,2);*/
  Dune::InverseOperatorResult stat;

  Dune::UMFPack<typename M::Container> direct_solver(native(m));

  // solve the jacobian system
  r *= -1.0; // need -residual
  DV x(gfs,0.0);
  //solvera.apply(native(x),native(r),stat);
  direct_solver.apply(native(x),native(r),stat);
  x += x0;

  // output grid function with VTKWriter
  Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x);
  vtkwriter.write(filename,Dune::VTK::ascii);

  typedef Dune::PDELab::DiscreteGridFunction<GFS,DV> DGF;
  DGF xdgf(x.gridFunctionSpace(),x);

  auto integrationWindow = Dune::PDELab::makeGridFunctionFromCallable (gv, [&](const auto& x){
    if (x[0] > 0.4 && x[0] < 0.6 && x[1] > 0.4 && x[1] < 0.6)
      return 1.0;
    return 0.0;
  });
  Dune::PDELab::ProductGridFunctionAdapter product(xdgf, integrationWindow);
  auto integral = Dune::PDELab::integrateGridFunction(product,4);

  std::vector<double> measurements(std::pow(measurement_per_dim,2));
  int c = 0;
  for (double meas_x = .5 / measurement_per_dim; meas_x < 1.0; meas_x+=1.0 / measurement_per_dim) {
    for (double meas_y = .5 / measurement_per_dim; meas_y < 1.0; meas_y+=1.0 / measurement_per_dim) {
      typename DGF::Traits::DomainType location;
      location[0] = meas_x;
      location[1] = meas_y;
      Dune::PDELab::GridFunctionProbe<DGF> probe(xdgf, location);
      typename DGF::Traits::RangeType val;
      probe.eval_all(val);
      measurements[c] = val;
      //std::cout << "(" << meas_x << "," << meas_y << ") " << val << std::endl;
      c++;
    }
  }
  if(c != measurements.size()) {
    std::cout << "Mismatch in number of probes taken and measurement dim.!" << std::endl;
    exit(42);
  }
  return std::make_pair(measurements, integral);
}

//===============================================================
// Main program with grid setup
//===============================================================

std::pair<std::vector<double>, double> run_pde(std::vector<std::reference_wrapper<const Eigen::VectorXd>> const& parameters, Eigen::VectorXd level)
{
  // YaspGrid Q2 2D test
  // make grid
  int gridsize[] = {8, 32, 128};
  /*if (level < 0 || level > 2) {
    std::cout << "Invalid level!" << std::endl;
    exit(42);
  }*/

  Dune::FieldVector<double,2> L(1.0);
  std::array<int,2> N = {gridsize[(int)level[0]], gridsize[(int)level[1]]};
  Dune::YaspGrid<2> grid(L,N);

  // get view
  typedef Dune::YaspGrid<2>::LeafGridView GV;
  const GV& gv=grid.leafGridView();

  // make finite element map
  typedef GV::Grid::ctype DF;
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,2> FEM;
  FEM fem(gv);

  // solve problem
  return poisson<GV,FEM,Dune::PDELab::ConformingDirichletConstraints>(gv,fem,"poisson_yasp_Q2_2d",2,parameters,N);
}


/*int param_dim(int layers) {
  return 2 * layers * (layers - 1) + 1;
}*/

class ExampleModPiece : public ShallowModPiece {
public:

  ExampleModPiece()
   : ShallowModPiece(Eigen::VectorXi::Ones(1)*4, (Eigen::Vector2i() << measurement_per_dim * measurement_per_dim, 1).finished())
  {
    outputs.push_back(Eigen::VectorXd::Ones(std::pow(measurement_per_dim,2)));
    outputs.push_back(Eigen::VectorXd::Ones(1));
  }

  void Evaluate(std::vector<std::reference_wrapper<const Eigen::VectorXd>> const& inputs, json config) override {

    std::vector<double> level_raw = config.value<std::vector<double>>("level", {0,0});
    Eigen::VectorXd level = stdvector_to_eigenvectord(level_raw);

    std::pair<std::vector<double>, double> pde_pair = run_pde(inputs, level);
    std::vector<double> measurements = pde_pair.first;
    double qoi = pde_pair.second;

    for (int i = 0; i < measurements.size(); i++) {
      outputs[0][i] = measurements[i];
    }
    outputs[1][0] = qoi;
  }

  bool SupportsEvaluate() override {
    return true;
  }

};

int main(int argc, char** argv){

  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
  std::cout << helper.size() << std::endl;


  char const* port_cstr = std::getenv("PORT");
  int port = 0;
  if ( port_cstr == NULL ) {
    std::cout << "Environment variable PORT not set! Using port 4242 as default." << std::endl;
    port = 4242;
  } else {
    port = atoi(port_cstr);
  }


  ExampleModPiece modPiece;

  serveModPiece(modPiece, "0.0.0.0", port);

  return 0;
}
